# teralytic-py

[![pipeline status](https://gitlab.com/teralytic/teralytic-py/badges/master/pipeline.svg)](https://gitlab.com/teralytic/teralytic-py/commits/master)

Teralytic Python SDK

This build is automated using swagger-codegen and the gitlab CI / CD pipelines.

## Manual install

To manually install this client you must have the pre-reqs installed:

1. docker 2.0.0+
2. curl
3. make
4. python pip

Clone the repo and run make

```bash
> git clone https://gitlab.com/teralytic/teralytic-py.git
> cd teralytic-py
> make
```
