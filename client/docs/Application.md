# Application

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The client id, if not provided one will be generated | [optional] 
**organization_id** | **str** | The organization this application belongs to | [optional] 
**name** | **str** | The application name | 
**client_secret** | **str** | The client secret, if not provided one will be generated | [optional] 
**expires_at** | **datetime** | The client expiration date | [optional] 
**scope** | [**StringArray**](StringArray.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


