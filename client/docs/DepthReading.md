# DepthReading

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**depth** | **int** | The depth value | [optional] 
**depth_units** | **str** | The depth units (i.e. in, cm) | [optional] 
**terascore** | **float** | TeraScore | [optional] 
**nitrogen** | **float** | Nitrogen | [optional] 
**nitrogen_ppm** | **float** | Nitrogen ppm | [optional] 
**phosphorus** | **float** | Phosphorus | [optional] 
**phosphorus_ppm** | **float** | Phosphorus ppm | [optional] 
**potassium** | **float** | Potassium | [optional] 
**potassium_ppm** | **float** | Potassium ppm | [optional] 
**ec** | **float** | Electrical Conductivity | [optional] 
**o2** | **float** | Dioxygen | [optional] 
**p_h** | **float** | pH Scale | [optional] 
**co2** | **float** | Carbon Dioxide | [optional] 
**awc** | **float** |  | [optional] 
**moisture** | **float** | Soil Moisture | [optional] 
**temperature** | **float** | Soil Temperature | [optional] 
**extended_attributes** | **dict(str, object)** | Additional properties | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


