# Field

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**organization_id** | **str** |  | [optional] 
**name** | **str** |  | 
**acreage** | **float** |  | [optional] 
**crop** | **str** |  | [optional] 
**geometry** | [**FieldGeometry**](FieldGeometry.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


