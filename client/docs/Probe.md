# Probe

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**serial_code** | **str** |  | [optional] 
**field_id** | **str** |  | [optional] 
**organization_id** | **str** |  | [optional] 
**location** | [**Location**](Location.md) |  | [optional] 
**texture** | [**StringMap**](StringMap.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


