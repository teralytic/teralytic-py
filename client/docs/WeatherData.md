# WeatherData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**summary** | **str** | Weather Summary | [optional] 
**precip_type** | **str** | Precipitation Type | [optional] 
**apparent_temp** | **float** | Apparent Temperature | [optional] 
**cloud_cover** | **float** | Cloud Cover | [optional] 
**dew_point** | **float** | Dew Point | [optional] 
**humidity** | **float** | Humidity | [optional] 
**ozone** | **float** | Ozone Rating | [optional] 
**precip_intensity** | **float** | Precipitation Intesity | [optional] 
**precip_probability** | **float** | Precipitation Probability | [optional] 
**pressure** | **float** | Barometric Pressure | [optional] 
**temperature** | **float** | Temperature | [optional] 
**time** | **datetime** | Weather Sample Time (epoch) | [optional] 
**location** | [**Location**](Location.md) |  | [optional] 
**uv_index** | **int** | UV Index | [optional] 
**wind_bearing** | **int** | Wind Bearing | [optional] 
**wind_gust** | **float** | Wind Gust | [optional] 
**wind_speed** | **float** | Wind Speed | [optional] 
**visibility** | **float** | Visibility | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


