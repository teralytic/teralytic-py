# teralytic.OrganizationApi

All URIs are relative to *https://api.teralytic.io/latest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**application_create**](OrganizationApi.md#application_create) | **POST** /organizations/{organization_id}/applications | Create a new organization client with the specified scope
[**application_delete**](OrganizationApi.md#application_delete) | **DELETE** /organizations/{organization_id}/applications/{client_id} | Delete an application api client
[**application_list**](OrganizationApi.md#application_list) | **GET** /organizations/{organization_id}/applications | Get organization applications
[**field_get**](OrganizationApi.md#field_get) | **GET** /organizations/{organization_id}/fields/{field_id} | List single Field details associated with Field id provided (from set of Fields associated with the organization)
[**field_list**](OrganizationApi.md#field_list) | **GET** /organizations/{organization_id}/fields | List all Fields associated with an organization
[**organization_get**](OrganizationApi.md#organization_get) | **GET** /organizations/{organization_id} | Get a specific organization
[**organization_list**](OrganizationApi.md#organization_list) | **GET** /organizations | List all Organizations
[**probe_get**](OrganizationApi.md#probe_get) | **GET** /organizations/{organization_id}/probes/{probe_id} | List single Probe details associated with Probe id provided (from set of Fields associated with the account key)
[**probe_list**](OrganizationApi.md#probe_list) | **GET** /organizations/{organization_id}/probes | List all Probes associated with an organization


# **application_create**
> Application application_create(organization_id, application=application)

Create a new organization client with the specified scope

Create a new third-party application client

### Example
```python
from __future__ import print_function
import time
import teralytic
from teralytic.rest import ApiException
from pprint import pprint

# Configure API key authorization: ApiKeyAuth
configuration = teralytic.Configuration()
configuration.api_key['x-api-key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['x-api-key'] = 'Bearer'
# Configure OAuth2 access token for authorization: OAuth2
configuration = teralytic.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = teralytic.OrganizationApi(teralytic.ApiClient(configuration))
organization_id = 'organization_id_example' # str | id of Organization for the operation
application = teralytic.Application() # Application | The application to create (optional)

try:
    # Create a new organization client with the specified scope
    api_response = api_instance.application_create(organization_id, application=application)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganizationApi->application_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_id** | [**str**](.md)| id of Organization for the operation | 
 **application** | [**Application**](Application.md)| The application to create | [optional] 

### Return type

[**Application**](Application.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **application_delete**
> application_delete(organization_id, client_id)

Delete an application api client

Delete an application client for the organization from the database

### Example
```python
from __future__ import print_function
import time
import teralytic
from teralytic.rest import ApiException
from pprint import pprint

# Configure API key authorization: ApiKeyAuth
configuration = teralytic.Configuration()
configuration.api_key['x-api-key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['x-api-key'] = 'Bearer'
# Configure OAuth2 access token for authorization: OAuth2
configuration = teralytic.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = teralytic.OrganizationApi(teralytic.ApiClient(configuration))
organization_id = 'organization_id_example' # str | id of Organization for the operation
client_id = 'client_id_example' # str | client_id of the application to delete

try:
    # Delete an application api client
    api_instance.application_delete(organization_id, client_id)
except ApiException as e:
    print("Exception when calling OrganizationApi->application_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_id** | [**str**](.md)| id of Organization for the operation | 
 **client_id** | [**str**](.md)| client_id of the application to delete | 

### Return type

void (empty response body)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **application_list**
> list[Application] application_list(organization_id)

Get organization applications

List all applications for an organization

### Example
```python
from __future__ import print_function
import time
import teralytic
from teralytic.rest import ApiException
from pprint import pprint

# Configure API key authorization: ApiKeyAuth
configuration = teralytic.Configuration()
configuration.api_key['x-api-key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['x-api-key'] = 'Bearer'
# Configure OAuth2 access token for authorization: OAuth2
configuration = teralytic.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = teralytic.OrganizationApi(teralytic.ApiClient(configuration))
organization_id = 'organization_id_example' # str | id of Organization for the operation

try:
    # Get organization applications
    api_response = api_instance.application_list(organization_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganizationApi->application_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_id** | [**str**](.md)| id of Organization for the operation | 

### Return type

[**list[Application]**](Application.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **field_get**
> Field field_get(organization_id, field_id)

List single Field details associated with Field id provided (from set of Fields associated with the organization)

### Example
```python
from __future__ import print_function
import time
import teralytic
from teralytic.rest import ApiException
from pprint import pprint

# Configure API key authorization: ApiKeyAuth
configuration = teralytic.Configuration()
configuration.api_key['x-api-key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['x-api-key'] = 'Bearer'
# Configure OAuth2 access token for authorization: OAuth2
configuration = teralytic.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = teralytic.OrganizationApi(teralytic.ApiClient(configuration))
organization_id = 'organization_id_example' # str | id of Organization to retrieve
field_id = 'field_id_example' # str | id of Field to retrieve

try:
    # List single Field details associated with Field id provided (from set of Fields associated with the organization)
    api_response = api_instance.field_get(organization_id, field_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganizationApi->field_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_id** | [**str**](.md)| id of Organization to retrieve | 
 **field_id** | [**str**](.md)| id of Field to retrieve | 

### Return type

[**Field**](Field.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **field_list**
> list[Field] field_list(organization_id, points=points)

List all Fields associated with an organization

### Example
```python
from __future__ import print_function
import time
import teralytic
from teralytic.rest import ApiException
from pprint import pprint

# Configure API key authorization: ApiKeyAuth
configuration = teralytic.Configuration()
configuration.api_key['x-api-key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['x-api-key'] = 'Bearer'
# Configure OAuth2 access token for authorization: OAuth2
configuration = teralytic.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = teralytic.OrganizationApi(teralytic.ApiClient(configuration))
organization_id = 'organization_id_example' # str | id of Organization to retrieve
points = ['points_example'] # list[str] | Array of points (lat,lng) describing a polygon search pattern. (optional)

try:
    # List all Fields associated with an organization
    api_response = api_instance.field_list(organization_id, points=points)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganizationApi->field_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_id** | [**str**](.md)| id of Organization to retrieve | 
 **points** | [**list[str]**](str.md)| Array of points (lat,lng) describing a polygon search pattern. | [optional] 

### Return type

[**list[Field]**](Field.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **organization_get**
> Organization organization_get(organization_id)

Get a specific organization

List single Organization details associated with Organization id provided

### Example
```python
from __future__ import print_function
import time
import teralytic
from teralytic.rest import ApiException
from pprint import pprint

# Configure API key authorization: ApiKeyAuth
configuration = teralytic.Configuration()
configuration.api_key['x-api-key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['x-api-key'] = 'Bearer'
# Configure OAuth2 access token for authorization: OAuth2
configuration = teralytic.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = teralytic.OrganizationApi(teralytic.ApiClient(configuration))
organization_id = 'organization_id_example' # str | id of Organization to retrieve

try:
    # Get a specific organization
    api_response = api_instance.organization_get(organization_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganizationApi->organization_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_id** | [**str**](.md)| id of Organization to retrieve | 

### Return type

[**Organization**](Organization.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **organization_list**
> list[Organization] organization_list()

List all Organizations

Lists all organization, requires admin scope

### Example
```python
from __future__ import print_function
import time
import teralytic
from teralytic.rest import ApiException
from pprint import pprint

# Configure API key authorization: ApiKeyAuth
configuration = teralytic.Configuration()
configuration.api_key['x-api-key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['x-api-key'] = 'Bearer'
# Configure OAuth2 access token for authorization: OAuth2
configuration = teralytic.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = teralytic.OrganizationApi(teralytic.ApiClient(configuration))

try:
    # List all Organizations
    api_response = api_instance.organization_list()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganizationApi->organization_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Organization]**](Organization.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **probe_get**
> Probe probe_get(organization_id, probe_id, texture=texture)

List single Probe details associated with Probe id provided (from set of Fields associated with the account key)

### Example
```python
from __future__ import print_function
import time
import teralytic
from teralytic.rest import ApiException
from pprint import pprint

# Configure API key authorization: ApiKeyAuth
configuration = teralytic.Configuration()
configuration.api_key['x-api-key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['x-api-key'] = 'Bearer'
# Configure OAuth2 access token for authorization: OAuth2
configuration = teralytic.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = teralytic.OrganizationApi(teralytic.ApiClient(configuration))
organization_id = 'organization_id_example' # str | id of Organization to retrieve
probe_id = 'probe_id_example' # str | id of Probe to retrieve
texture = true # bool | query soil texture data (optional)

try:
    # List single Probe details associated with Probe id provided (from set of Fields associated with the account key)
    api_response = api_instance.probe_get(organization_id, probe_id, texture=texture)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganizationApi->probe_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_id** | [**str**](.md)| id of Organization to retrieve | 
 **probe_id** | **str**| id of Probe to retrieve | 
 **texture** | **bool**| query soil texture data | [optional] 

### Return type

[**Probe**](Probe.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **probe_list**
> list[Probe] probe_list(organization_id, fields=fields)

List all Probes associated with an organization

### Example
```python
from __future__ import print_function
import time
import teralytic
from teralytic.rest import ApiException
from pprint import pprint

# Configure API key authorization: ApiKeyAuth
configuration = teralytic.Configuration()
configuration.api_key['x-api-key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['x-api-key'] = 'Bearer'
# Configure OAuth2 access token for authorization: OAuth2
configuration = teralytic.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = teralytic.OrganizationApi(teralytic.ApiClient(configuration))
organization_id = 'organization_id_example' # str | id of Organization to retrieve
fields = ['fields_example'] # list[str] | Fields to filter search on (optional)

try:
    # List all Probes associated with an organization
    api_response = api_instance.probe_list(organization_id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganizationApi->probe_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_id** | [**str**](.md)| id of Organization to retrieve | 
 **fields** | [**list[str]**](str.md)| Fields to filter search on | [optional] 

### Return type

[**list[Probe]**](Probe.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

