# Reading

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**organization_id** | **str** | The field id of the probe | [optional] 
**field_id** | **str** | The field id of the probe | [optional] 
**probe** | [**Probe**](Probe.md) |  | [optional] 
**timestamp** | **datetime** | Time when Reading was measured | [optional] 
**location** | [**Feature**](Feature.md) |  | [optional] 
**irrigation** | [**IrrigationData**](IrrigationData.md) |  | [optional] 
**microclimate** | [**MicroClimate**](MicroClimate.md) |  | [optional] 
**readings** | [**list[DepthReading]**](DepthReading.md) | probe depth readings | [optional] 
**profile** | **str** | The calibration profile | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


