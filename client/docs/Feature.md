# Feature

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **object** |  | [optional] 
**type** | [**FeatureType**](FeatureType.md) |  | [optional] 
**geometry** | [**Geometry**](Geometry.md) |  | [optional] 
**bbox** | **list[float]** | Bounding box of the feature | [optional] 
**properties** | **dict(str, object)** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


