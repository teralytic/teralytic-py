# teralytic.WeatherApi

All URIs are relative to *https://api.teralytic.io/latest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**weather_query**](WeatherApi.md#weather_query) | **GET** /weather | Get historical weather data


# **weather_query**
> list[WeatherData] weather_query(location, start_date=start_date, end_date=end_date, limit=limit)

Get historical weather data

Query the weather data for the location

### Example
```python
from __future__ import print_function
import time
import teralytic
from teralytic.rest import ApiException
from pprint import pprint

# Configure API key authorization: ApiKeyAuth
configuration = teralytic.Configuration()
configuration.api_key['x-api-key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['x-api-key'] = 'Bearer'
# Configure OAuth2 access token for authorization: OAuth2
configuration = teralytic.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = teralytic.WeatherApi(teralytic.ApiClient(configuration))
location = 'location_example' # str | A bounding geohash of <= 5 bytes or a wkt POINT that will be converted to a geohash with 5 byte precision. 
start_date = 'start_date_example' # str | Start date and time for the query in RFC3339 format, the default is 24h prior  (optional)
end_date = 'end_date_example' # str | End date and time for the query in RFC3339 format, the implied default is now  (optional)
limit = 100 # int | Limit the number of readings per probe returned. The default is 24 hours of readings The maximum is 7 days of readings  (optional) (default to 100)

try:
    # Get historical weather data
    api_response = api_instance.weather_query(location, start_date=start_date, end_date=end_date, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling WeatherApi->weather_query: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **location** | **str**| A bounding geohash of &lt;&#x3D; 5 bytes or a wkt POINT that will be converted to a geohash with 5 byte precision.  | 
 **start_date** | **str**| Start date and time for the query in RFC3339 format, the default is 24h prior  | [optional] 
 **end_date** | **str**| End date and time for the query in RFC3339 format, the implied default is now  | [optional] 
 **limit** | **int**| Limit the number of readings per probe returned. The default is 24 hours of readings The maximum is 7 days of readings  | [optional] [default to 100]

### Return type

[**list[WeatherData]**](WeatherData.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

