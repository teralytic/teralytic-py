# IrrigationData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**schedule** | **int** | Irrigation Schedule | [optional] 
**vol_acre_in** | **float** | Irrigation volume in3/acre | [optional] 
**vol_acre_cm** | **float** | Irrigation volume c3/acre | [optional] 
**gross_in** | **float** | Gross irrigation in in3 | [optional] 
**gross_cm** | **float** | Gross irrigation in cm3 | [optional] 
**net_in** | **float** | Net irrigation in in3 | [optional] 
**net_cm** | **float** | Net irrigation in cm3 | [optional] 
**recommendation** | **str** | Irrigation recommendation | [optional] 
**stream** | **float** |  | [optional] 
**aw_6** | **float** | available water at 6 inches | [optional] 
**aw_18** | **float** | available water at 18 inches | [optional] 
**aw_36** | **float** | available water at 36 inches | [optional] 
**aw_total** | **float** | total available water | [optional] 
**fc_6** | **float** | field capacity at 6 inches | [optional] 
**fc_18** | **float** | field capacity at 18 inches | [optional] 
**fc_36** | **float** | field capacity at 36 inches | [optional] 
**pwp_6** | **float** | Permanent Wilting Point at 6 inches | [optional] 
**pwp_18** | **float** | Permanent Wilting Point at 18 inches | [optional] 
**pwp_36** | **float** | Permanent Wilting Point at 36 inches | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


