# MicroClimate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**humidity** | **float** |  | [optional] 
**lux** | **float** |  | [optional] 
**temperature** | **float** | Air Temperature | [optional] 
**eto** | **float** | Evaporative Transpiration | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


