# teralytic.TelemetryApi

All URIs are relative to *https://api.teralytic.io/latest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**readings_query**](TelemetryApi.md#readings_query) | **GET** /organizations/{organization_id}/readings | Query sensor readings


# **readings_query**
> list[Reading] readings_query(organization_id, start_date, end_date=end_date, fields=fields, probes=probes, location=location, properties=properties, extended=extended, options=options, operation=operation, sample_rate=sample_rate, fill=fill, limit=limit, offset=offset, profile=profile, sort=sort)

Query sensor readings

Query sensor readings associated with organization

### Example
```python
from __future__ import print_function
import time
import teralytic
from teralytic.rest import ApiException
from pprint import pprint

# Configure API key authorization: ApiKeyAuth
configuration = teralytic.Configuration()
configuration.api_key['x-api-key'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['x-api-key'] = 'Bearer'
# Configure OAuth2 access token for authorization: OAuth2
configuration = teralytic.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = teralytic.TelemetryApi(teralytic.ApiClient(configuration))
organization_id = 'organization_id_example' # str | id of Organization to retrieve
start_date = 'start_date_example' # str | Start date and time for the query format. Specifying `first` will return the oldest available data.  Example - `2019-03-15T00:00:00-00:00` 
end_date = 'end_date_example' # str | End date and time for the query, the implied default is now  Example - `2019-03-15T00:00:00-00:00`  (optional)
fields = ['fields_example'] # list[str] | The fields to return readings for (optional)
probes = ['probes_example'] # list[str] | The probes to return readings for (optional)
location = 'location_example' # str | A bounding geohash of <= 7 bytes or a lat/lng string pair '-40.357589721679 175.60340881347' that will be convert to a geohash with 7 byte precision.  (optional)
properties = ['properties_example'] # list[str] | The properties to return readings for (optional)
extended = false # bool | Return extended attributes (optional) (default to false)
options = 'options_example' # str | Options for the call (optional)
operation = 'last' # str | The analytics operations to perform (optional) (default to last)
sample_rate = '1h' # str | The query sample rate (optional) (default to 1h)
fill = 'none' # str | The missing value fill method  (optional) (default to none)
limit = 24 # int | Limit the number of readings to return per probe, the max is 7 days,  the default is 24 hours.  (optional) (default to 24)
offset = 0 # int | offset into the query to return  (optional) (default to 0)
profile = 'default' # str | Specify the calibration profile  (optional) (default to default)
sort = 'ASC' # str | Specify the sort field for the results.  * ASC - ascending and group by timestamp * DEC - decending and group by timestamp * NONE - sort by full group by tags which effectively results in sorting by organization, field, and location.  (optional) (default to ASC)

try:
    # Query sensor readings
    api_response = api_instance.readings_query(organization_id, start_date, end_date=end_date, fields=fields, probes=probes, location=location, properties=properties, extended=extended, options=options, operation=operation, sample_rate=sample_rate, fill=fill, limit=limit, offset=offset, profile=profile, sort=sort)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TelemetryApi->readings_query: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_id** | [**str**](.md)| id of Organization to retrieve | 
 **start_date** | **str**| Start date and time for the query format. Specifying &#x60;first&#x60; will return the oldest available data.  Example - &#x60;2019-03-15T00:00:00-00:00&#x60;  | 
 **end_date** | **str**| End date and time for the query, the implied default is now  Example - &#x60;2019-03-15T00:00:00-00:00&#x60;  | [optional] 
 **fields** | [**list[str]**](str.md)| The fields to return readings for | [optional] 
 **probes** | [**list[str]**](str.md)| The probes to return readings for | [optional] 
 **location** | **str**| A bounding geohash of &lt;&#x3D; 7 bytes or a lat/lng string pair &#39;-40.357589721679 175.60340881347&#39; that will be convert to a geohash with 7 byte precision.  | [optional] 
 **properties** | [**list[str]**](str.md)| The properties to return readings for | [optional] 
 **extended** | **bool**| Return extended attributes | [optional] [default to false]
 **options** | **str**| Options for the call | [optional] 
 **operation** | **str**| The analytics operations to perform | [optional] [default to last]
 **sample_rate** | **str**| The query sample rate | [optional] [default to 1h]
 **fill** | **str**| The missing value fill method  | [optional] [default to none]
 **limit** | **int**| Limit the number of readings to return per probe, the max is 7 days,  the default is 24 hours.  | [optional] [default to 24]
 **offset** | **int**| offset into the query to return  | [optional] [default to 0]
 **profile** | **str**| Specify the calibration profile  | [optional] [default to default]
 **sort** | **str**| Specify the sort field for the results.  * ASC - ascending and group by timestamp * DEC - decending and group by timestamp * NONE - sort by full group by tags which effectively results in sorting by organization, field, and location.  | [optional] [default to ASC]

### Return type

[**list[Reading]**](Reading.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth), [OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/csv, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

