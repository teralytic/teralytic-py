# coding: utf-8

"""
    teralytic

    The Teralytic API allows clients to manage their organization, view their fields and and probes, and query sensor readings and analytics.  For sandbox testing you may use the api key: `swagger.teralytic.io`   # noqa: E501

    OpenAPI spec version: 1.1.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class IrrigationData(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'schedule': 'int',
        'vol_acre_in': 'float',
        'vol_acre_cm': 'float',
        'gross_in': 'float',
        'gross_cm': 'float',
        'net_in': 'float',
        'net_cm': 'float',
        'recommendation': 'str',
        'stream': 'float',
        'aw_6': 'float',
        'aw_18': 'float',
        'aw_36': 'float',
        'aw_total': 'float',
        'fc_6': 'float',
        'fc_18': 'float',
        'fc_36': 'float',
        'pwp_6': 'float',
        'pwp_18': 'float',
        'pwp_36': 'float'
    }

    attribute_map = {
        'schedule': 'schedule',
        'vol_acre_in': 'vol_acre_in',
        'vol_acre_cm': 'vol_acre_cm',
        'gross_in': 'gross_in',
        'gross_cm': 'gross_cm',
        'net_in': 'net_in',
        'net_cm': 'net_cm',
        'recommendation': 'recommendation',
        'stream': 'stream',
        'aw_6': 'aw_6',
        'aw_18': 'aw_18',
        'aw_36': 'aw_36',
        'aw_total': 'aw_total',
        'fc_6': 'fc_6',
        'fc_18': 'fc_18',
        'fc_36': 'fc_36',
        'pwp_6': 'pwp_6',
        'pwp_18': 'pwp_18',
        'pwp_36': 'pwp_36'
    }

    def __init__(self, schedule=None, vol_acre_in=None, vol_acre_cm=None, gross_in=None, gross_cm=None, net_in=None, net_cm=None, recommendation=None, stream=None, aw_6=None, aw_18=None, aw_36=None, aw_total=None, fc_6=None, fc_18=None, fc_36=None, pwp_6=None, pwp_18=None, pwp_36=None):  # noqa: E501
        """IrrigationData - a model defined in Swagger"""  # noqa: E501

        self._schedule = None
        self._vol_acre_in = None
        self._vol_acre_cm = None
        self._gross_in = None
        self._gross_cm = None
        self._net_in = None
        self._net_cm = None
        self._recommendation = None
        self._stream = None
        self._aw_6 = None
        self._aw_18 = None
        self._aw_36 = None
        self._aw_total = None
        self._fc_6 = None
        self._fc_18 = None
        self._fc_36 = None
        self._pwp_6 = None
        self._pwp_18 = None
        self._pwp_36 = None
        self.discriminator = None

        if schedule is not None:
            self.schedule = schedule
        if vol_acre_in is not None:
            self.vol_acre_in = vol_acre_in
        if vol_acre_cm is not None:
            self.vol_acre_cm = vol_acre_cm
        if gross_in is not None:
            self.gross_in = gross_in
        if gross_cm is not None:
            self.gross_cm = gross_cm
        if net_in is not None:
            self.net_in = net_in
        if net_cm is not None:
            self.net_cm = net_cm
        if recommendation is not None:
            self.recommendation = recommendation
        if stream is not None:
            self.stream = stream
        if aw_6 is not None:
            self.aw_6 = aw_6
        if aw_18 is not None:
            self.aw_18 = aw_18
        if aw_36 is not None:
            self.aw_36 = aw_36
        if aw_total is not None:
            self.aw_total = aw_total
        if fc_6 is not None:
            self.fc_6 = fc_6
        if fc_18 is not None:
            self.fc_18 = fc_18
        if fc_36 is not None:
            self.fc_36 = fc_36
        if pwp_6 is not None:
            self.pwp_6 = pwp_6
        if pwp_18 is not None:
            self.pwp_18 = pwp_18
        if pwp_36 is not None:
            self.pwp_36 = pwp_36

    @property
    def schedule(self):
        """Gets the schedule of this IrrigationData.  # noqa: E501

        Irrigation Schedule  # noqa: E501

        :return: The schedule of this IrrigationData.  # noqa: E501
        :rtype: int
        """
        return self._schedule

    @schedule.setter
    def schedule(self, schedule):
        """Sets the schedule of this IrrigationData.

        Irrigation Schedule  # noqa: E501

        :param schedule: The schedule of this IrrigationData.  # noqa: E501
        :type: int
        """

        self._schedule = schedule

    @property
    def vol_acre_in(self):
        """Gets the vol_acre_in of this IrrigationData.  # noqa: E501

        Irrigation volume in3/acre  # noqa: E501

        :return: The vol_acre_in of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._vol_acre_in

    @vol_acre_in.setter
    def vol_acre_in(self, vol_acre_in):
        """Sets the vol_acre_in of this IrrigationData.

        Irrigation volume in3/acre  # noqa: E501

        :param vol_acre_in: The vol_acre_in of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._vol_acre_in = vol_acre_in

    @property
    def vol_acre_cm(self):
        """Gets the vol_acre_cm of this IrrigationData.  # noqa: E501

        Irrigation volume c3/acre  # noqa: E501

        :return: The vol_acre_cm of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._vol_acre_cm

    @vol_acre_cm.setter
    def vol_acre_cm(self, vol_acre_cm):
        """Sets the vol_acre_cm of this IrrigationData.

        Irrigation volume c3/acre  # noqa: E501

        :param vol_acre_cm: The vol_acre_cm of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._vol_acre_cm = vol_acre_cm

    @property
    def gross_in(self):
        """Gets the gross_in of this IrrigationData.  # noqa: E501

        Gross irrigation in in3  # noqa: E501

        :return: The gross_in of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._gross_in

    @gross_in.setter
    def gross_in(self, gross_in):
        """Sets the gross_in of this IrrigationData.

        Gross irrigation in in3  # noqa: E501

        :param gross_in: The gross_in of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._gross_in = gross_in

    @property
    def gross_cm(self):
        """Gets the gross_cm of this IrrigationData.  # noqa: E501

        Gross irrigation in cm3  # noqa: E501

        :return: The gross_cm of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._gross_cm

    @gross_cm.setter
    def gross_cm(self, gross_cm):
        """Sets the gross_cm of this IrrigationData.

        Gross irrigation in cm3  # noqa: E501

        :param gross_cm: The gross_cm of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._gross_cm = gross_cm

    @property
    def net_in(self):
        """Gets the net_in of this IrrigationData.  # noqa: E501

        Net irrigation in in3  # noqa: E501

        :return: The net_in of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._net_in

    @net_in.setter
    def net_in(self, net_in):
        """Sets the net_in of this IrrigationData.

        Net irrigation in in3  # noqa: E501

        :param net_in: The net_in of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._net_in = net_in

    @property
    def net_cm(self):
        """Gets the net_cm of this IrrigationData.  # noqa: E501

        Net irrigation in cm3  # noqa: E501

        :return: The net_cm of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._net_cm

    @net_cm.setter
    def net_cm(self, net_cm):
        """Sets the net_cm of this IrrigationData.

        Net irrigation in cm3  # noqa: E501

        :param net_cm: The net_cm of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._net_cm = net_cm

    @property
    def recommendation(self):
        """Gets the recommendation of this IrrigationData.  # noqa: E501

        Irrigation recommendation  # noqa: E501

        :return: The recommendation of this IrrigationData.  # noqa: E501
        :rtype: str
        """
        return self._recommendation

    @recommendation.setter
    def recommendation(self, recommendation):
        """Sets the recommendation of this IrrigationData.

        Irrigation recommendation  # noqa: E501

        :param recommendation: The recommendation of this IrrigationData.  # noqa: E501
        :type: str
        """

        self._recommendation = recommendation

    @property
    def stream(self):
        """Gets the stream of this IrrigationData.  # noqa: E501


        :return: The stream of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._stream

    @stream.setter
    def stream(self, stream):
        """Sets the stream of this IrrigationData.


        :param stream: The stream of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._stream = stream

    @property
    def aw_6(self):
        """Gets the aw_6 of this IrrigationData.  # noqa: E501

        available water at 6 inches  # noqa: E501

        :return: The aw_6 of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._aw_6

    @aw_6.setter
    def aw_6(self, aw_6):
        """Sets the aw_6 of this IrrigationData.

        available water at 6 inches  # noqa: E501

        :param aw_6: The aw_6 of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._aw_6 = aw_6

    @property
    def aw_18(self):
        """Gets the aw_18 of this IrrigationData.  # noqa: E501

        available water at 18 inches  # noqa: E501

        :return: The aw_18 of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._aw_18

    @aw_18.setter
    def aw_18(self, aw_18):
        """Sets the aw_18 of this IrrigationData.

        available water at 18 inches  # noqa: E501

        :param aw_18: The aw_18 of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._aw_18 = aw_18

    @property
    def aw_36(self):
        """Gets the aw_36 of this IrrigationData.  # noqa: E501

        available water at 36 inches  # noqa: E501

        :return: The aw_36 of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._aw_36

    @aw_36.setter
    def aw_36(self, aw_36):
        """Sets the aw_36 of this IrrigationData.

        available water at 36 inches  # noqa: E501

        :param aw_36: The aw_36 of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._aw_36 = aw_36

    @property
    def aw_total(self):
        """Gets the aw_total of this IrrigationData.  # noqa: E501

        total available water  # noqa: E501

        :return: The aw_total of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._aw_total

    @aw_total.setter
    def aw_total(self, aw_total):
        """Sets the aw_total of this IrrigationData.

        total available water  # noqa: E501

        :param aw_total: The aw_total of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._aw_total = aw_total

    @property
    def fc_6(self):
        """Gets the fc_6 of this IrrigationData.  # noqa: E501

        field capacity at 6 inches  # noqa: E501

        :return: The fc_6 of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._fc_6

    @fc_6.setter
    def fc_6(self, fc_6):
        """Sets the fc_6 of this IrrigationData.

        field capacity at 6 inches  # noqa: E501

        :param fc_6: The fc_6 of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._fc_6 = fc_6

    @property
    def fc_18(self):
        """Gets the fc_18 of this IrrigationData.  # noqa: E501

        field capacity at 18 inches  # noqa: E501

        :return: The fc_18 of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._fc_18

    @fc_18.setter
    def fc_18(self, fc_18):
        """Sets the fc_18 of this IrrigationData.

        field capacity at 18 inches  # noqa: E501

        :param fc_18: The fc_18 of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._fc_18 = fc_18

    @property
    def fc_36(self):
        """Gets the fc_36 of this IrrigationData.  # noqa: E501

        field capacity at 36 inches  # noqa: E501

        :return: The fc_36 of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._fc_36

    @fc_36.setter
    def fc_36(self, fc_36):
        """Sets the fc_36 of this IrrigationData.

        field capacity at 36 inches  # noqa: E501

        :param fc_36: The fc_36 of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._fc_36 = fc_36

    @property
    def pwp_6(self):
        """Gets the pwp_6 of this IrrigationData.  # noqa: E501

        Permanent Wilting Point at 6 inches  # noqa: E501

        :return: The pwp_6 of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._pwp_6

    @pwp_6.setter
    def pwp_6(self, pwp_6):
        """Sets the pwp_6 of this IrrigationData.

        Permanent Wilting Point at 6 inches  # noqa: E501

        :param pwp_6: The pwp_6 of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._pwp_6 = pwp_6

    @property
    def pwp_18(self):
        """Gets the pwp_18 of this IrrigationData.  # noqa: E501

        Permanent Wilting Point at 18 inches  # noqa: E501

        :return: The pwp_18 of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._pwp_18

    @pwp_18.setter
    def pwp_18(self, pwp_18):
        """Sets the pwp_18 of this IrrigationData.

        Permanent Wilting Point at 18 inches  # noqa: E501

        :param pwp_18: The pwp_18 of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._pwp_18 = pwp_18

    @property
    def pwp_36(self):
        """Gets the pwp_36 of this IrrigationData.  # noqa: E501

        Permanent Wilting Point at 36 inches  # noqa: E501

        :return: The pwp_36 of this IrrigationData.  # noqa: E501
        :rtype: float
        """
        return self._pwp_36

    @pwp_36.setter
    def pwp_36(self, pwp_36):
        """Sets the pwp_36 of this IrrigationData.

        Permanent Wilting Point at 36 inches  # noqa: E501

        :param pwp_36: The pwp_36 of this IrrigationData.  # noqa: E501
        :type: float
        """

        self._pwp_36 = pwp_36

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(IrrigationData, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, IrrigationData):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
