SWAGGER_SPEC ?= "https://api.swaggerhub.com/apis/Teralytic/teralytic-api/1.1.0/swagger.yaml"

ifeq ($(OS),Windows_NT)
	PWD ?= $(shell cd)
endif

default: install

fetch:
	curl -o swagger.yaml $(SWAGGER_SPEC)

build: fetch
	docker run --rm -v ${PWD}:/local swaggerapi/swagger-codegen-cli:2.4.4 generate -l python -i /local/swagger.yaml -t /local/templates -c /local/config.json -o /local/client

install: build
	cd client && pip install .

.PHONY: \
	install \
	build \
	fetch